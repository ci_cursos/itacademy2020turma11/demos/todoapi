using System;
using System.ComponentModel.DataAnnotations;

namespace todoapi
{
    public class TodoItem
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }
        public string Description {get; set;}
        public bool IsComplete { get; set; }
    }
}